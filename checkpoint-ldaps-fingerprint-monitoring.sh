#!/bin/bash

###########################################################################################
# Script Name  : checkpoint-ldaps-fingerprint-monitoring.sh
# Description  : Monitors the certificate fingerprint of the given LDAPs servers
# Args         : None, change variables on top of this script before use
# Author       : tbi88
# URL          : https://gitlab.com/tbi88/checkpoint-ldaps-fingerprint-monitoring
###########################################################################################

. /etc/profile.d/CP.sh

# Mail Alert settings
TO_ADDRESS="fwadmin@company.com"
FROM_ADDRESS="firewall@company.com"
SUBJECT="LDAPs Fingerprint has changed!"
MAILSERVER="mail.company.com"

SERVERS_JSON='{
  "servers": [
    {
      "server": "ldap1.comapny.com",
      "port": "636",
      "fingerprint": "A1:CC:2B:F7:C4:6E:85:D0:52:12:73:91:EB:85:3B:61"
    },
    {
      "server": "ldap2.company.com",
      "port": "636",
      "fingerprint": "95:B1:06:28:EB:4B:24:C6:EE:5C:28:4C:C9:A6:19:F7"
    }
  ]
}'

while read server; do
  SERVER=`echo $server | jq -r '.["server"]'`
  FINGERPRINT=`echo $server | jq -r '.["fingerprint"]'`
  PORT=`echo $server | jq -r '.["port"]'`

  # connect to server and fetch the certificate
  cpopenssl s_client -connect $SERVER:$PORT </dev/null 2>/dev/null | sed -n '/-----BEGIN CERTIFICATE-----/,/-----END CERTIFICATE-----/p' > cert.crt

  # calculate fingerprint from the certificate
  FINGERPRINT_NEW=`cpopenssl x509 -noout -fingerprint -md5 -inform pem -in cert.crt | awk -F '=' '{print $2}'`

  # some fancy output
  echo "SERVER:                  $SERVER"
  echo "Fingerprint should be:   $FINGERPRINT"
  echo "Fingerprint is:          $FINGERPRINT_NEW"

  # compare the fingerprints
  if [ "$FINGERPRINT" == $FINGERPRINT_NEW ]; then
    echo "Fingerprints are matching!"
  else
    echo "Fingerprint has changed on the Server!"

    # Body of the mail
    BODY="The LDAPs fingerprint on the server $SERVER has changed.
    old fingerprint was: $FINGERPRINT
    new fingerprint is : $FINGERPRINT_NEW

    Please do the following:
    1. Open SmartConsole
    2. In the Object Explorer go to: Servers --> LDAP Account Units --> <Your LDAP Account Unit>
    3. Switch to the Servers tab and open the server $SERVER
    4. Switch to the Encryption tab and click on fetch to get the new fingerprint
    5. Make sure the fingerprint has changed and save the change
    6. Install the policy on all relevant gateways
    7. Change the fingerprint in the /home/admin/scripts/checkpoint-ldaps-fingerprint-monitoring.sh script on the Management server"

    # send the email alert
    echo -e "$BODY" | $FWDIR/bin/sendmail -t $MAILSERVER -s "$SUBJECT" -f $FROM_ADDRESS $TO_ADDRESS
   fi

  # cleaning up
  rm cert.crt

  echo ""

done <<< "$(echo $SERVERS_JSON | jq -c '.servers[]')"
