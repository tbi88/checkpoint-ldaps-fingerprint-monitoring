# checkpoint-ldaps-fingerprint-monitoring

A script to monitor LDAPs fingerprints on a Checkpoint Gateway/Management.

Tested on Checkpoint R80.X.

Usage:
1. Put the script onto your Management server or Gateway.
2. Change the variables on top of the script.
3. Add a cron job to run the script at least once a day.
4. Make sure the script is working by adding a wrong fingerprint at the start.

The script does not check if the new certificate is valid. So make sure to check the certificate before you trust the new fingerprint.
